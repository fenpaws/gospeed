# Measure your ISPs speed and visualize it with Grafana 

This tool helps you to gather information about the stability and speed of your Internet connection by using the Ooklas new CLI Speedtestclient.

This tool does NOT use the `speedtest-cli` package, use the [official Ookal Speedtest CLi](https://www.speedtest.net/de/apps/cli).

---

### There are two ways to use this tool:

### 1. Run the script directly

#### Dependencies

You need Ookla Speedtest CLI, you can download it directly from their [Website](https://www.speedtest.net/de/apps/cli) or use your package manager. Again this is not speedtest-cli!

#### Configuration

```bash
export INFLUX_HOST=[hostname/ip]
export INFLUX_PORT=[port, uses default port 8086 if not specified ]
export INFLUX_USERNAME=[username]
export INFLUX_PASSWORD=[password]
export INFLUX_DBNAME=[database name]

export TAG_HOST=[host]
export TAG_REGION=[region]
export TAG_LOCATION=[location]

export SAVE_JSON=[true/false]
```

#### Run

```bash
chmod +x gospeedtest
.\gospeedtest
```


### 2. Use docker

CLone this repository and change the following entries in the `docker-compose.yml`

#### config

```yml
 environment:
    - INFLUX_HOST=[hostname/ip]
    - INFLUX_PORT=[port, uses default port 8086 if not specified ]
    - INFLUX_USERNAME=[username]
    - INFLUX_PASSWORD=[password]
    - INFLUX_DBNAME=[database name]

    - TAG_HOST=[host]
    - TAG_REGION=[region]
    - TAG_LOCATION=[location]

    - SAVE_JSON=[true/false]

```

after that run `docker-compose up -d` to start the container.

## ADDONS

### Dashboard

I created a Grafana dashboard to display your results.  

just import the [Dasboard](dashboard.json) to your Grafana instance and you should be ready to go.

![Dasboard](dashboard.jpg)