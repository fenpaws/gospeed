package models

import "time"

// SpeedTest represent that main object that gives back from the speedtest.net cli tool
type SpeedTest struct {
	Type         string       `json:"type"` // This can olny be "result"
	Timestamp    time.Time    `json:"timestamp"`
	Ping         Ping         `json:"ping"`
	Download     UpDowm       `json:"download"`
	Upload       UpDowm       `json:"upload"`
	PacketLoss   float32      `json:"packetLoss"`
	ISP          string       `json:"isp"`
	NetInterface NetInterface `json:"interface"`
	Server       Server       `json:"server"`
	Result       Result       `json:"result"`
}

type Ping struct {
	Jitter  float32 `json:"jitter"`
	Latency float32 `json:"latency"`
}

type UpDowm struct {
	Bandwidth int64 `json:"bandwidth"`
	Bytes     int64 `json:"bytes"`
	Elepsed   int64 `json:"elepsed"`
}

type NetInterface struct {
	InternalIP string `json:"internalIp"`
	Name       string `json:"name"`
	MacAddress string `json:"macAddr"`
	IsVPN      bool   `json:"isVpn"`
	ExternalIP string `json:"externalIp"`
}

type Server struct {
	ID       int16  `json:"id"`
	Name     string `json:"name"`
	Location string `json:"location"`
	Country  string `json:"country"`
	Host     string `json:"host"`
	Port     int16  `json:"port"`
	IP       string `json:"ip"`
}

type Result struct {
	ID  string `json:"id"`
	URL string `json:"url"`
}
