package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"time"

	"github.com/caarlos0/env/v6"
	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/fenpaws/gospeed/models"
)

// config for the GOSpeedtest, reads from environment
type config struct {
	Server       string `env:"INFLUX_HOST,required"`          // InfluxDB Server address
	Port         int    `env:"INFLUX_PORT" envDefault:"8086"` // InfluxDB port
	Username     string `env:"INFLUX_USERNAME,required"`      // InfluxDB Username
	Password     string `env:"INFLUX_PASSWORD,required"`      // InfluxDB User password
	DatabaseName string `env:"INFLUX_DBNAME,required"`        // InfluxDB database name
	Host         string `env:"TAG_HOST"`                      // Host identifier
	Region       string `env:"TAG_REGION"`                    // Host region
	Location     string `env:"TAG_LOCATION"`                  // host location
	SaveJson     bool   `env:"SAVE_JSON" envDefault:"false"`  // save generated json file
}

func main() {

	for {
		cfg := config{}
		if err := env.Parse(&cfg); err != nil {
			log.Fatalf("error while parsing config: %v\n", err)
		}

		speedtest, err := getCurSpeed()
		if err != nil {
			log.Fatalf("problem while getting results from speedtest: %v", err.Error())
		}

		c, err := client.NewHTTPClient(client.HTTPConfig{
			Addr:     fmt.Sprintf("http://%s:%d", cfg.Server, cfg.Port),
			Username: cfg.Username,
			Password: cfg.Password,
		})
		if err != nil {
			log.Fatalln("Error creating InfluxDB Client: ", err.Error())
		}
		defer c.Close()

		ping, version, err := c.Ping(20 * time.Second)

		if err != nil {
			log.Fatal("Problem while pinging the database: ", err)
		}

		log.Printf("Ping to InfluxDB: %dms", ping.Milliseconds())
		log.Printf("InfluxDB Version: %s", version)

		tags := make(map[string]string)
		fields := make(map[string]interface{})

		tags["isp"] = speedtest.ISP
		tags["host"] = cfg.Host
		tags["location"] = cfg.Location
		tags["region"] = cfg.Region

		fields["download"] = speedtest.Download.Bandwidth * 8
		fields["upload"] = speedtest.Upload.Bandwidth * 8
		fields["ping"] = speedtest.Ping.Latency
		fields["packetLoss"] = speedtest.PacketLoss
		fields["result_id"] = speedtest.Result.ID
		fields["remote_server_host"] = speedtest.Server.Host
		fields["remote_server_region"] = speedtest.Server.Country

		bpc := client.BatchPointsConfig{
			Database:         cfg.DatabaseName,
			RetentionPolicy:  "autogen",
			WriteConsistency: "any",
			Precision:        "ns",
		}
		bp, err := client.NewBatchPoints(bpc)
		if err != nil {
			log.Fatalf("Problem while creationg new BatchPints: ", err)
		}

		p, err := client.NewPoint("speedtest", tags, fields, speedtest.Timestamp)
		if err != nil {
			log.Fatalf("Problem while creating a new Point: ", err)
		}

		bp.AddPoint(p)
		err = c.Write(bp)

		if err != nil {
			log.Fatal(err)
		}

		if cfg.SaveJson == true {
			err := saveJsonData(speedtest)
			if err != nil {
				log.Println("error while writing json file: ", err)
			}
		}
		time.Sleep(1 * time.Hour)
	}

}

// TODO comment getCurSpeed
func getCurSpeed() (*models.SpeedTest, error) {
	cmd := exec.Command("speedtest", "-f", "json", "--accept-license", "--accept-gdpr")
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	log.Println(string(out))

	var speedtest models.SpeedTest
	err = json.Unmarshal(out, &speedtest)

	if err != nil {
		return nil, err
	}
	return &speedtest, nil
}

// saveJsonData save the json input from SpeedTest to a Json file
func saveJsonData(SpeedTest *models.SpeedTest) error {
	file, err := json.MarshalIndent(SpeedTest, "", "4")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile("test.json", file, 0644)
	if err != nil {
		return err
	}
	log.Println("Writing Json complete")
	return nil
}
