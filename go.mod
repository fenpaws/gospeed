module gitlab.com/fenpaws/gospeed

go 1.14

require (
	github.com/caarlos0/env/v6 v6.2.1
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
)
